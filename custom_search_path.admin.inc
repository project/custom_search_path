
<?php

// Build the admin settings form.
function custom_search_path_admin_settings_form() {
  $form = array();

  $form['custom_search_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom search path string'),
    '#default_value' => variable_get('custom_search_path', 'content'),
    '#description' => t("Enter and save a custom string to replace \"apachesolr_search\" in Apache Solr default search path \"search/apachesolr_search\". For the new custom search path string to take effect, please also <a href=\"/admin/settings/performance\">clear the website's cache</a>. If you encounter any problems with Custom Search Path module, then please read its <a href=\"http://drupal.org/project/issues/custom_search_path\">issue queue </a> and, if necessary, create a new issue."),
  );

  return system_settings_form($form);
}

