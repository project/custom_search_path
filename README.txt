Description
-----------
This simple module replaces default search path 'search/apachesolr_search/%' 
by Apache Solr Search Integration module with more appealing
'search/your-custom-string/%'.

Requirements
------------
Drupal 6
Apache Solr Search Integration module

Installation
------------
1. Download and extract the recommended version of the module from 
http://drupal.org/project/custom_search_path to modules directory of your Drupal setup.

2. Login as an administrator. 
Enable the module in the "Administer" -> "Site Building" -> "Modules" (admin/build/modules).

3. Go to '/admin/settings/apachesolr/path/' and set a 
custom string to replace default 'apachesolr_search'.

4. Clear the cache of your website.


